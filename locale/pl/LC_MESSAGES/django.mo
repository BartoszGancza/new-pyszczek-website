��    %      D  5   l      @     A     J     S     a     q     �     �  =   �     �     �     �     �                              #     1  *   :     e     n     �     �     �  t   �     %  $   *  *   O  ,   z  8   �  3   �  *     !   ?     a     z  �  }     {     �     �     �     �     �     �  6   �  
   	     	     *	     2	     :	     G	  	   P	     Z	     a	     j	  
   	  3   �	     �	     �	     �	     �	     
  h   
     }
  )   �
  "   �
  %   �
  /   �
  +   ,  '   X     �     �     �                                              %   !                       $      "                	                    #      
                                                           About Us About us Change E-mail Change Password Change Speech Details Confirm Password Reset Contact E-mail with password reset instructions was sent successfully Events Find Us! Login Logout New e-mail: News Next Page Place: Post via bot? Previous PySzczek Szczecin Python User's Group 2019 Register Register as a Speaker Register as a speaker Reset Password Submit The password reset link was invalid, possibly because it has already been used. Please request a new password reset. User Which event do you want to speak on: You have successfully changed your e-mail! You have successfully changed your password! You have successfully changed your presentation details! You have successfully created your speaker profile! You have successfully reset your password! You were logged out successfully! Your presentation title: of Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-07-05 14:01+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 O Nas O Nas Zmień E-mail Zmień Hasło Edytuj Wystąpienie Potwierdź Reset Hasła Kontakt Pomyślnie wysłano e-mail z instrukcją resetu hasła Wydarzenia Dołącz do Nas! Zaloguj Wyloguj Nowy e-mail: Nowości Następne Strona Miejsce: Powiadomienie botem? Poprzednie Grupa Użytkowników Pythona PySzczek Szczecin 2019 Zarejestruj Zarejestruj Wystąpienie Zarejestruj Wystąpienie Zresetuj Hasło Wyślij Link do resetu hasla jest nieważny, ponieważ być może został już użyty. Zresetuj hasło ponownie. Użytkownik Na ktorym evencie chciałbyś wystąpić: Pomyślnie zmieniono adres e-mail! Zmiana hasła przebiegła pomyślnie! Pomyślnie zmieniono informacje o wystąpieniu! Profil mówcy został pomyślnie utworzony! Pomyślnie zresetowałeś swoje hasło! Nastąpiło wylogowanie! Tytuł prezentacji: z 
# PySzczek Website
This project is created and maintained by PySzczek - Szczecin's Python Users Group
an informal group of python enthusiasts from city of Szczecin (Poland)

It's purpose is to create a website that will later be used for communication and spreading information about our group.

## Run the project

### Using docker

In order to run the project you need to first build the image:
```bash
docker build . -t pyszczek_website
```

Then you can run the container:
```bash
docker run -d -e SECRET_KEY=123 -e GOOGLE_MAPS_API_KEY=123 -e DATABASE_URL=sqlite:///db.sqlite3 -e DEBUG=True -p 8000:8000 --name pyszczek_website pyszczek_website
```

If you need to stop it, run following command:
```bash
docker stop pyszczek_website
```

Running the container again can be done with command:
```bash
docker start pyszczek_website
```

### Running project natively
Please use Python 3.7

Create virtual environment
```bash
pip install virtualenv
virtualenv -p python3 venv
```

Remember to activate virtualenv before starting the project, you can do it with command
```bash
source venv/bin/activate
```

Install requirements
```bash
pip install -r requirements.txt
```

Set following environment variables:
```bash
export SECRET_KEY=123
export GOOGLE_MAPS_API_KEY=123
export DATABASE_URL=sqlite:///db.sqlite3
export DEBUG=True
```

Run migrations:
```bash
python3 manage.py migrate
```

Run development server
```bash
python3 manage.py runserver
```

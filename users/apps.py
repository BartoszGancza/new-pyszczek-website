from django.apps import AppConfig


class UserControlConfig(AppConfig):
    name = 'users'

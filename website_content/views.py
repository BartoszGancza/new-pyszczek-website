from django.contrib import messages
from django.shortcuts import redirect
from django.utils import translation
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView

from website.settings import GOOGLE_MAPS_API_KEY
from website_content.decorators import check_for_language_cookie
from website_content.models import Article, AboutUs, Event
from users.models import Profile


@method_decorator(check_for_language_cookie, name='dispatch')
class IndexListView(ListView):
    template_name = 'aboutus.html'
    model = Profile
    paginate_by = 6

    def get_queryset(self):
        session = self.request.session
        return super(IndexListView, self).get_queryset().filter(user__is_staff=True).translate(
            session[translation.LANGUAGE_SESSION_KEY])

    def get_context_data(self, **kwargs):
        session = self.request.session
        context = super(IndexListView, self).get_context_data()
        about_us_text = AboutUs.objects.all().translate(session[translation.LANGUAGE_SESSION_KEY])
        if about_us_text:
            context['about_us_text'] = about_us_text[0].text
        return context


@method_decorator(check_for_language_cookie, name='dispatch')
class ArticleListView(ListView):
    template_name = 'articles.html'
    model = Article
    paginate_by = 9


@method_decorator(check_for_language_cookie, name='dispatch')
class SingleArticleView(TemplateView):
    template_name = 'singlearticle.html'

    def get_context_data(self, **kwargs):
        session = self.request.session
        context = super().get_context_data(**kwargs)
        context['article_data'] = Article.objects.filter(id=self.kwargs['id']).translate(
            session[translation.LANGUAGE_SESSION_KEY])
        return context


@method_decorator(check_for_language_cookie, name='dispatch')
class ContactView(TemplateView):
    template_name = 'contact.html'


@method_decorator(check_for_language_cookie, name='dispatch')
class EventListView(ListView):
    template_name = 'events.html'
    model = Event
    paginate_by = 9

    def get_queryset(self):
        session = self.request.session
        return super(EventListView, self).get_queryset().translate(session[translation.LANGUAGE_SESSION_KEY])


@method_decorator(check_for_language_cookie, name='dispatch')
class SingleEventView(TemplateView):
    template_name = 'singleevent.html'

    def get_context_data(self, **kwargs):
        session = self.request.session
        context = super().get_context_data(**kwargs)
        context['event_data'] = Event.objects.filter(id=self.kwargs['id']).translate(
            session[translation.LANGUAGE_SESSION_KEY])
        context['GOOGLE_MAPS_API_KEY'] = GOOGLE_MAPS_API_KEY
        context['geocode'] = Event.objects.filter(id=self.kwargs['id']).values_list('geolocation', flat=True)
        return context


def set_page_language(request, **kwargs):
    if kwargs['lang'] == 'en':
        user_language = 'en'
        translation.activate(user_language)
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        messages.success(request, 'Language is now set to English')
        return redirect(request.GET['next'])
    else:
        user_language = 'pl'
        translation.activate(user_language)
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        messages.success(request, 'Język jest teraz ustawiony na Polski')
        return redirect(request.GET['next'])

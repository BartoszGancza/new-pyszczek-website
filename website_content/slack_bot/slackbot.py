import os
import slack


def message_as_bot(instance):
    message_content = f'Na naszej stronie pojawił się nowy artykuł!\n\nTytuł: "{instance.title}"\n' \
                      f'<https://pyszczek-website.herokuapp.com/index?id={instance.id}|*Kliknij tu aby ' \
                      f'przejść do strony.*>'
    client = slack.WebClient(token=os.environ['SLACK_API_TOKEN'])

    client.chat_postMessage(
        channel='#bot_tests',
        text=message_content
    )

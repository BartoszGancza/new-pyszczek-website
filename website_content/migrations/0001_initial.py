# Generated by Django 2.1.4 on 2019-10-22 14:13

from django.conf import settings
from django.db import migrations, models
import django_google_maps.fields
import markdownx.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256)),
                ('photo', models.URLField(blank=True)),
                ('photo_description', models.CharField(blank=True, max_length=256)),
                ('body', markdownx.models.MarkdownxField()),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('bot_post', models.BooleanField(verbose_name='Post via bot?')),
                ('author', models.ForeignKey(on_delete='cascade', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256)),
                ('description', markdownx.models.MarkdownxField()),
                ('agenda', markdownx.models.MarkdownxField()),
                ('event_date', models.DateTimeField()),
                ('photo', models.URLField(blank=True)),
                ('address', django_google_maps.fields.AddressField(max_length=200)),
                ('geolocation', django_google_maps.fields.GeoLocationField(max_length=100)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
    ]

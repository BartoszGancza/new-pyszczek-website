from django.conf.urls import include
from django.urls import reverse_lazy, path
from django.views.generic import RedirectView
from markdownx import urls as markdownx

from . import views

urlpatterns = [
    path('', RedirectView.as_view(url=reverse_lazy('aboutus'))),
    path('aboutus/', views.IndexListView.as_view(), name='aboutus'),
    path('news/', views.ArticleListView.as_view(), name='news'),
    path('news/<id>/', views.SingleArticleView.as_view(), name='single_article'),
    path('contact/', views.ContactView.as_view(), name='contact'),
    path('events/', views.EventListView.as_view(), name='events'),
    path('events/<id>/', views.SingleEventView.as_view(), name='single_event'),
    path('lang/<lang>/', views.set_page_language, name='language'),
    path('markdownx/', include(markdownx)),
]

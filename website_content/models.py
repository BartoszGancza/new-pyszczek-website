from django.contrib.auth.models import User
from django.db import models
from translations.models import Translatable
from markdownx.models import MarkdownxField
from django_google_maps import fields as map_fields
from django.utils.translation import gettext_lazy as _


class Event(Translatable):
    title = models.CharField(max_length=256)
    description = MarkdownxField()
    agenda = MarkdownxField()
    event_date = models.DateTimeField()
    photo = models.URLField(blank=True)
    address = map_fields.AddressField(max_length=200)
    geolocation = map_fields.GeoLocationField(max_length=100)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['id']

    class TranslatableMeta:
        fields = ['title', 'description', 'agenda']


class Article(Translatable):
    title = models.CharField(max_length=256)
    photo = models.URLField(blank=True)
    photo_description = models.CharField(max_length=256, blank=True)
    body = MarkdownxField()
    author = models.ForeignKey(User, on_delete='cascade')
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    bot_post = models.BooleanField(verbose_name=_('Post via bot?'))

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['id']

    class TranslatableMeta:
        fields = ['title', 'body', 'photo_description']


class AboutUs(Translatable):
    text = models.TextField()

    class TranslatableMeta:
        fields = ['text']
